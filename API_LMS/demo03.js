const express = require('express')
var products = require('./data.json')

const app = express();
const port = 3000;

app.listen(port, (req, res) => {
    console.log(`Example app listening http://localhost:${port}`)
})

app.get('/products', (req, res) => {
    res.status = 200;
    res.setHeader('Content-Type', 'application/json')
    res.send(products);
})

app.get('/products/:productID', (req, res) => {
    let id = req.params['productID'];
    res.status = 200;
    res.setHeader('Content-Type', 'application/json')
    res.send(products[id - 1]);
})