const express = require('express');
const app = express();
const port = 3000;

app.listen(port, () => {
    console.log(`example app listening at http:localhost:${port}`);

})

app.get('/', (req, res) => {

    res.send('GET method for READ data');

})

app.post('/', (req, res) => {

    res.send('POST method for CREATE data');

})

app.put('/', (req, res) => {
    res.send('PUT method for UPDATE data')
})
app.delete('/', (req, res) => {
    res.send('DELETE method for DELETE data');
})